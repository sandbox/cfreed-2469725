<?php
/**
 * @file
 * Multilang module text data.
 *
 * Contains all texts translations, written using "multi" syntax.
 */

/* ================================================================== INSTALL */
/**
 * Informative message after module activation.
 */
function _multilang_install() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]The <em>Multilang</em> module was successfully installed.<br />
Now you may use the "multi" syntax inside of text fields, in any type of 
content and in blocks.<br />
NOTE: in order to use the
<a href="<?php echo _MULBASE_PATH; ?>/en/admin/help/multilang#ckeditor">
advanced features</a>
available with CKEditor, you must install the <em>Widget</em> plugin.
[fr]Le module <em>Multilang</em> a été installé avec succès.<br />
Vous pouvez désormais utiliser la syntaxe "multi" à l\'intérieur des champs
texte, dans n\'importe quel type de contenu ainsi que dans les blocs.<br />
NOTA : pour utiliser les
<a href="<?php echo _MULBASE_PATH; ?>/fr/admin/help/multilang#ckeditor">
possibilités avancées</a> disponibles avec CKEditor, vous devez installer le
plugin <em>Widget</em>.
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ================================================================= FALLBACK */
/**
 * Tip displayed when a "multi" block doesn't contain the required language.
 */
function _multilang_fallback() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]This section is not translated yet. We apologize for any inconvenience.
[fr]Cette section n'a pas encore été traduite. Merci de votre compréhension.
[/multi]
<?php
  // Special case: default=NULL notifies not to fallback when required
  // language is not found (or would enter infinite loop).
  return _multilang_process(ob_get_clean(), /*links*/FALSE, /*default*/NULL);
}
/* =================================================================== TOKENS */
/**
 * Localized tokens characteristics.
 */
function _multilang_token_info() {
  return array(
    'multilang-title' => array(
      'name' => '
[multi]
[en]Title with Multilang
[fr]Titre avec Multilang
[/multi]
      ',
      'description' => '
[multi]
[en]The node title, localized with Multilang
[fr]Le titre du noeud, localisé avec Multilang
[/multi]
      ',
    ),
    'multilang-native-title' => array(
      'name' => '
[multi]
[en]Title (native) with Multilang
[fr]Titre (natif) avec Multilang
[/multi]
      ',
      'description' => '
[multi]
[en]The node title, in the default site language with Multilang
[fr]Le titre du noeud, dans la langue du site par défaut avec Multilang
[/multi]
      ',
    ),
  );
}
/* ================================================================= CKEDITOR */
/**
 * Informative text displayed in CKEditor config plugins list.
 */
function _multilang_ckeditor_plugin() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]Multilang - Plugin to embed multiple languages translations in the same 
field
[fr]Multilang - Un plugin pour intégrer des traductions en plusieurs langues 
dans le même champ
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ===================================================================== HELP */
/**
 * Complete documentation.
 */
function _multilang_help() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
<h3>
  <em>Multilang</em>
</h3>
[multi]
[en]
<p>
  This module is intended to allow entering all desired translations of a given
  content inside of a unique node, instead of having to create a separate node
  (with <em>Multilingual Content</em>) or a distinct field version (with
  <em>Entity Translation</em>) for each translation.
</p>
<p>
  The benefits of using this method are mainly:
  <ul>
    <li>when entering new content or updating an existing one, authors keep a
    total visibility upon all the translations at the same time</li>
    <li>a direct consequence is to reduce the risk of omitting to update the
    translations when an original content has been modified</li>
    <li>when a content includes a lot of lang-insensitive data (like images,
    links, or simply verbous HTML with a number of attributes), these parts
    don't have to be duplicated: only the textual parts must be entered as
    "multi" and rewritten in the different languages</li>
    <li>since it emulates the Spip &lt;multi&gt; syntax, this method is nicely
    suitable to allow the direct migration of contents from this CMS, without
    the need of a painful data restructuration</li>
    <li>moreover, in the latter case, authors can maintain their previous work
    habits unchanged</li>
  </ul>
</p>
<div>
<h3 class="multilang-toggler" id="install">
  Installation/Configuration
</h3>
<p>
  <em>Multilang</em> works without the need of other translation module,
  assumed you have set the "Internationalization" option of <em>i18n</em>, and
  configured the desired languages. Nevertheless you can simultaneously use
  alternative translation methods, like <em>Multilingual Content</em>,
  <em>Menu translation</em> etc.
</p>
<p>
  By itself, the module contains no configuration option. By cons, to use the
  "multi" syntax you must:
  <ul>
    <li>
      In "Administer > Configuration > Regional and language > Languages",
      enable the "URL" detection method, and configure it to "Path prefix"</li>
    <li>
      For each enabled language, ensure to provide the corresponding standard
      "Path prefix language code" (don't leave it empty)</li>
    <li>
      Position the <em>Language switcher (User interface text)</em> block in a
      region, anywhere you like</li>
  </ul>
</p>
<p>
  If you're also using other options of the <em>i18n</em> module, there are
  some additional configuration options you must pay attention to:
  <ul>
    <li>
      for each node where the "multi" syntax is used, you MUST select the
      <strong>Neutral language</strong> in the "Language" field (or the lang
      switcher would deactivate any lang other than the specified one)</li>
    <li>
      for each block where the the "multi" syntax is used, you MUST
      <strong>keep unchecked all the proposed languages</strong> in the
      "Languages" part of "Visibility settings"</li>
  </ul>
</p>
</div><div>
<h3 class="multilang-toggler" id="syntax">
  Syntax
</h3>
<p>
  When entering data in a text area, you may insert multilingual pieces of
  text by enclosing them between <code>&#91;multi&#93;</code> and
  <code>&#91;/multi&#93;</code> tags.<br />
  Such a piece of text may contain a number of blocks, each representing the
  desired content translated in a given language, like
  <code>[language-mark]&hellip;content&hellip;</code>, where:
  <ul>
    <li>
      <code>language-mark</code> is the involved language code, such as
      <code>en</code>, <code>fr</code>&hellip;</li>
    <li>
      <code>&hellip;content&hellip;</code> is the translated content</li>
  </ul>
</p>
<p>
  Example:<br />
  <blockquote class="multilang-code"><code>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;/multi&#93;
  </code></blockquote>
</p>
<p>
  Any spaces, newlines, line-breaks or paragraph-breaks are ignored when they
  are located just after the opening tags, juste before the closing tags, and
  just around the language-marks. In other words, each text block is rendered
  trimmed.
</p>
</div><div>
<h3 class="multilang-toggler" id="pathauto">
  Using with <em>Pathauto</em>
</h3>
<p>
  With the <em>Pathauto</em> module, if you have introduced "multi" syntax in
  the node titles, you may use the
  <code>[node:multilang-native-title]</code> token to generate URL aliases,
  which will be localized using the <strong>site default language</strong>.
  <br />
  CAUTION: using the <code>[node:title]</code> token would generate aliases
  from the <strong>raw</strong> title, resulting in something like
  <code>multienmy-titlefrmon-titremulti</code>!</li>
</p>
</div><div>
<h3 class="multilang-toggler" id="views">
  Using with <em>Views</em>
</h3>
<p>
  With the <em>Views</em> module, you can use the "multi" syntax inside of the
  texts you enter in the definition forms of a view.<br />
  They will be rendered like explained above, in the views summary, in the
  previews, and of course in the pages where they are included.
</p>
</div><div>
<h3 class="multilang-toggler" id="ckeditor">
  Using with <em>CKEditor</em>
</h3>
<p>
  With the <em>CKEditor</em> module, you can benefit from improved input method
  which gets you rid of the above syntax and automatically offers input areas
  dedicated to each of the languages defined for the site.<br />
  For this to work:
  <ul>
    <li>
      your version of <em>CKEditor</em> must include the <em>Widget</em> plugin
      (you may install it from the <a href="http://ckeditor.com/builder">
      CKEditor builder</a>)</li>
    <li>
      in the <em>CKEditor</em> configuration, for each profile where you want
      to allow it, in the "EDITOR'S APPEARANCE" group:
      <ol>
        <li>
          in the section "Tools bar", add the "Multilang" button to the tools
          bar</li>
        <li>
          in the section "Plugins", check "Multilang" in the list of plugins
          to be activated</li>
      </ol>
    </li>
    <li>
      then with this profile any text field part where the "multi" syntax is
      used automatically displays a "MULTILANG" group, with a subgroup inside
      for each defined language</li>
    <li>
      at any time you may click the "Multilang" button in the tools bar to
      create a new empty "MULTILANG" group</li>
    <li>
      note that only text fields (or blocks text) can be entered through
      <em>CKEDITOR</em>: in views, and in nodes or blocks titles, you still
      must use the "multi" syntax</li>
</p>
</div><div>
<h3 class="multilang-toggler" id="notes">
  Notes
</h3>
<ol>
  <li>
    Which language code is used to render depends on the lang part of the
    current URL (such as "en" in <code>http://example.com/en/...</code>), which
    is generally defined by how the lang switcher is currently set. If no 
    language is currently defined (so Drupal language is empty), the site lang 
    is used.<br />
    As a fortunate side effect, at any moment you may deactivate the lang
    switcher, and all contents including the "multi" syntax are simply rendered
    in the site lang.<br />
    CAUTION: at the time this document is written (Drupal 7.34), deactivate the
    lang switcher seems to cause vocabulary terms translations to be lost!</li>
  <li>
    If a "multi" block does not contain translation for the current lang, the
    available text in the site lang will be rendered instead.</li>
  <li>
    You may use the "multi" syntax not only in any text field or block body, but
    also in the node or block title.</li>
  <li>
    In order to allow a simple migration of contents from the Spip CMS, an
    alternative syntax is also accepted, using HTML-fashion tags like
    <code>&lt;multi&gt;</code>, rather than <code>&#91;multi&#93;</code>.<br />
    You may also use this syntax when manually entering text (using plain text
    editor).
    </li>
</ol>
</div><div>
<h3 class="multilang-toggler" id="localization">
  Localization
</h3>
<p>
  Because of its mission to integrate multiple languages in the same container,
  the <em>Multilang</em> module does not conform to the standard Drupal
   localization system: on this level, it is self-sufficient and embeds all
   of its own translations, written with the "multi" syntax.<br />
   All these translations (including the text you are reading now) are
   gathered in the file <code>multilang.data.inc</code>.
</p>
<p>
  To add a language, you just have to extend this file, in which the various
  "multi" blocks required are divided into a few number of functions,
  standardized according to the following scheme:
  <blockquote class="multilang-code"><code>
    function _multilang_FUNCTION() {<br />
    &nbsp;&nbsp;module_load_include('inc', 'multilang', 'multilang.core');<br />
    &nbsp;&nbsp;ob_start();<br />
    ?&gt;<br />
    <strong>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;/multi&#93;<br />
    </strong>
    &lt;?php<br />
    &nbsp;&nbsp;return _multilang_process(ob_get_clean());<br />
    }
  </code></blockquote>
</p>
<p>
  In the example above, to add a Spanish translation will require you insert it
  in the "multi" block, which will become:
  <blockquote class="multilang-code"><code>
    <strong>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;es&#93;Este es un texto en español<br />
    &#91;/multi&#93;<br />
    </strong>
  </code></blockquote>
</p>
<p>
  The <em>_multilang_token_info()</em> function is an exception to the above
  diagram, because the text it contains is distributed in a
  two-dimensional array, where each element is a "multi" block. It is within 
  these that you will insert the new translations.
</p>
<p>
  <em>NOTE FOR ENGLISH SPEAKERS: the creator of this module is not 
  fluent in English, so it is unsure of the quality of writing. <b>All 
  corrections and improvements are welcome.</b></em>
</p>
</div>
[fr]
<p>
  Ce module est destiné à permettre la saisie de toutes les traductions 
  souhaitées, pour un texte donné, au sein d'un noeud unique, au lieu de 
  devoir créer pour chaque traduction un nouveau noeud (avec
  <em>Multilingual Content</em>) ou une version séparée de champ (avec
  <em>Entity Translation</em>).
</p>
<p>
  Cette méthode présente plusieurs avantages, et notamment :
  <ul>
    <li>
      lorsqu'il saisit ou modifie un contenu, l'auteur conserve à tout moment 
      une vision globale sur toutes les traductions existantes</li>
    <li>
      consequence directe, on réduit le risque d'oublier de mettre à jour une 
      traduction lorsque le contenu original est modifié</li>
    <li>
      lorsqu'un contenu comporte une part de données qui ne changent pas avec 
      la langue (des images, des liens, ou simplement du HTML avec de nombreux 
      attributs), ces données n'ont pas besoin d'être dupliquées : seules les 
      parties textuelles doivent être saisies comme "multi" et ré-écrites dans
      les différentes langues</li>
    <li>
      parce qu'elle émule la syntaxe Spip &lt;multi&gt;, cette méthode est 
      particulièrement adaptée pour une migration directe des contenus issus 
      de ce CMS, évitant ainsi un pénible travail de restructuration</li>
    <li>
      en outre, dans ce cas, les auteurs peuvent conserver leurs habitudes de 
      travail antérieures</li>
  </ul>
</p>
<div>
<h3 class="multilang-toggler" id="install">
  Installation/Configuration
</h3>
<p>
  <em>Multilang</em> fonctionne sans recourir à un autre module voué aux 
  traductions, dès l'instant où vous avez activé l'option
  "Internationalisation" de <em>i18n</em>, et configuré les langages souhaités.
  Cependant vous pouvez tout de même utiliser simultanément une ou plusieurs
  méthodes de traduction alternatives, comme <em>Multilingual Content</em>,
  <em>Menu translation</em> etc.
</p>
<p>
  Par lui-même, le module ne nécessite aucune configuration. Par contre, pour
  utiliser la syntaxe "multi" vous devrez :
  <ul>
    <li>
      dans "Administration > Configuration > Regionalisation et langue > 
      Langues", activer la méthode de détection "URL" et la configurer pour 
      utiliser "Préfixe de chemin"</li>
    <li>
      pour chaque langue activée, vous assurer d'avoir bien saisi le code
      standard correspondant dans la zone "Code de langue du préfixe de 
      chemin" (ne la laissez pas vide)</li>
    <li>
      placer le bloc <em>Sélecteur de langue (Texte de l'interface utilisateur)
      </em> dans une région de votre choix</li>
  </ul>
</p>
<p>
  Si vous utilisez également d'autres options du module <em>i18n</em>, vous
  devrez également prêter attention aux options de configuration suivantes :
  <ul>
    <li>
      pour chaque noeud dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      sélectionner <strong>Independant de la langue</strong> dans le champ
      "Langue" (sinon le sélecteur de langue désactiverait toutes les langues
      autres que celle spécifiée)</li>
    <li>
      pour chaque bloc dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      <strong>ne sélectionner aucune des langues proposées</strong> dans la
      partie "Langues" du groupe "Paramètres de visibilité"</li>
  </ul>
</p>
</div><div>
<h3 class="multilang-toggler" id="syntax">
  Syntaxe
</h3>
<p>
  Lorsque vous saisissez des données dans un champ de type texte, vous pouvez 
  insérer des portions de texte multilingues en les enfermant entre des 
  balises <code>&#91;multi&#93;</code> et <code>&#91;/multi&#93;</code>.<br />
  Une telle portion de texte peut contenir un certain nombre de blocs, chacun 
  représentant le contenu souhaité, traduit dans une langue donnée, sous la 
  forme <code>[marqueur de langue]&hellip;contenu&hellip;</code>, où :
  <ul>
    <li>
      <code>marqueur de langue</code> est le code de langue concerné, par
      exemple <code>en</code>, <code>fr</code>&hellip;</li>
    <li>
      <code>&hellip;contenu&hellip;</code> est le contenu traduit</li>
  </ul>
</p>
<p>
  Exemple:<br />
  <blockquote class="multilang-code"><code>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;/multi&#93;
  </code></blockquote>
</p>
<p>
  Tous les espaces, caractères "nouvelle ligne", sauts de ligne ou sauts de 
  paragraphe HTML sont ignorés lorsqu'il se trouvent juste après les balises 
  ouvrantes, juste avant les balises fermantes, et précisément autour des 
  marques de code de langue. En d'autres termes, chaque bloc de texte est 
  rendu "nettoyé".
</p>
</div><div>
<h3 class="multilang-toggler" id="pathauto">
  Utilisation avec <em>Pathauto</em>
</h3>
<p>
  Avec le module <em>Pathauto</em> module, si vous avez introduit la syntaxe
  "multi" dans le titre des noeuds, vous pouvez utiliser le jeton
  <code>[node:multilang-native-title]</code> pour générer des alias d'URL, qui
  seront localisés dans la <strong>langue par défaut du site</strong>.<br />
  ATTENTION : utiliser le jeton <code>[node:title]</code> générerait les alias
  à partir du <strong>texte brut</strong> du titre, pour donner quelque chose
  comme <code>multienmy-titlefrmon-titremulti</code> !</li>
</p>
</div><div>
<h3 class="multilang-toggler" id="views">
  Utilisation avec <em>Views</em>
</h3>
<p>
  Avec le module <em>Views</em>, vous pouvez utiliser la syntaxe "multi" au
  sein des textes saisis dans les formulaires de définition d'une vue.<br />
  Ils seront rendus selon les règles exposées ci-dessus, dans le sommaire des
  vues, dans les aperçus, et bien sûr dans les pages où elles sont incluses.
</p>
</div><div>
<h3 class="multilang-toggler" id="ckeditor">
  Utilisation avec <em>CKEditor</em>
</h3>
<p>
  Avec le module <em>CKEditor</em>, vous pouvez bénéficier d'un mode de saisie
  amélioré qui fait abstraction de la syntaxe ci-dessus et vous propose
  automatiquement des zones de saisie dédiées à chacune des langues définies
  pour le site.<br />
  Pour utiliser cette possibilité :
  <ul>
    <li>
      votre version de <em>CKEditor</em> doit inclure le plugin <em>Widget</em>
      (vous pouvez l'installer à partir du
      <a href="http://ckeditor.com/builder">constructeur <em>CKEditor</em></a>)
      </li>
    <li>
      dans la configuration de <em>CKEditor</em>, pour chaque profil dans
      lequel vous voulez l'autoriser, dans le groupe "APPARENCE DE L'EDITEUR :
      <ol>
        <li>
          dans la section "Barre d'outils", ajoutez le bouton "Multilang" à
          la barre d'outils</li>
        <li>
          dans la section "Plugins", cochez "Multilang" dans la liste des
          plugins à activer</li>
      </ol>
    </li>
    <li>
      désormais, dans ce profil, toute partie de champ texte utilisant la
      syntaxe "multi" fait automatiquement apparaître un groupe "MULTILANG",
      à l'intérieur duquel un sous-groupe est affecté à chaque langue
      définie</li>
    <li>
      à tout moment, vous pouvez cliquer sur le bouton "Multilang" de la barre
      d'outils pour créer un nouveau groupe "MULTILANG" vide</li>
    <li>
      notez que seuls les champs texte (ou le texte des blocs) peuvent être
      saisis sous <em>CKEDITOR</em> : dans les vues, et dans les titres de
      noeuds ou de blocs, vous devrez toujours utiliser la syntaxe "multi"</li>
  </ul>
</p>
</div><div>
<h3 class="multilang-toggler" id="notes">
  Notes
</h3>
<ol>
  <li>
    Le code de langue choisi par le filtre pour extraire le texte dépend de la
    partie "langue" de l'URL actuelle (par exemple "fr" dans
    <code>http://exemple.com/fr/...</code>), qui est généralement définie par
    la situation actuelle du sélecteur de langue. Si aucune langue n'est 
    actuellement définie (la langue actuelle pour Drupal est donc vide), c'est
    la langue du site qui est utilisée.<br />
    Cet "effet de bord" intéressant vous permet à tout moment de désactiver le 
    sélecteur de langue, de telle sorte que tous les contenus utilisant le
    module Multilang seront simplement rendus dans la langue du site.<br />
    ATTENTION: au moment où ce document est rédigé (Drupal 7.34), il semble que
    la désactivation du sélecteur de langue provoque la perte des traductions
    des termes de taxonomie !</li>
  <li>
    Si un bloc "multi" ne possède aucune traduction pour la langue actuelle,
    c'est le texte disponible dans la langue du site qui sera affiché.</li>
  <li>
    Vous pouvez utiliser la syntaxe "multi" dans n'importe quel champ texte ou
    corps de bloc, mais aussi dans le titre d'un noeud ou d'un bloc.</li>
  <li>
    Pour une migration simplifiée de données en provenance du CMS Spip, une 
    syntaxe alternative est également acceptée, utilisant des balises du type
    HTML comme <code>&lt;multi&gt;</code> à la place de
    <code>&#91;multi&#93;</code>.<br />
    Vous pouvez aussi utiliser cette syntaxe lorsque vous saisissez du texte
    manuellement (hors <em>CKEditor</em>).</li>
</ol>
</div><div>
<h3 class="multilang-toggler" id="localization">
  Localisation
</h3>
<p>
  Du fait de sa vocation à intégrer plusieurs langues dans un même conteneur,
  le module <em>Multilang</em> ne se conforme pas à la norme Drupal en matière
  de localisation : sur ce plan, il est auto-suffisant et embarque la totalité
  de ses propres traductions, rédigées avec la syntaxe "multi".<br />
  Toutes ces traductions (y compris l'aide que vous lisez en ce moment) sont
  rassemblées dans le fichier <code>multilang.data.inc</code>.
</p>
<p>
  Pour ajouter une langue, il suffit donc d'enrichir ce fichier, dans lequel
  les différents blocs "multi" nécessaires sont répartis dans un petit nombre
  de fonctions, standardisées selon le schéma suivant :
  <blockquote class="multilang-code"><code>
    function _multilang_FUNCTION() {<br />
    &nbsp;&nbsp;module_load_include('inc', 'multilang', 'multilang.core');<br />
    &nbsp;&nbsp;ob_start();<br />
    ?&gt;<br />
    <strong>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;/multi&#93;<br />
    </strong>
    &lt;?php<br />
    &nbsp;&nbsp;return _multilang_process(ob_get_clean());<br />
    }
  </code></blockquote>
</p>
<p>
  Dans l'exemple ci-dessus, pour ajouter une traduction en espagnol, il faudra
  l'insérer dans le bloc "multi", qui deviendra :
  <blockquote class="multilang-code"><code>
    <strong>
    &#91;multi&#93;<br />
    &#91;en&#93;This is an english text<br />
    &#91;fr&#93;Ceci est un texte en fran&ccedil;ais<br />
    &#91;es&#93;Este es un texto en español<br />
    &#91;/multi&#93;<br />
    </strong>
  </code></blockquote>
</p>
<p>
  La fonction <em>_multilang_token_info()</em> constitue une exception au
  schema ci-dessus, car les textes qu'elle contient sont répartis dans un
  tableau à deux dimensions, où chaque élément est un bloc "multi". C'est à 
  l'intérieur de ceux-ci qu'il faudra insérer les nouvelles traductions.
</p>
</div>
[/multi]
<script>
(function($) {
$(document).ready(function() {
  $('head').append('\
<style>\
.multilang-toggler {\
  cursor: pointer;\
}\
.multilang-toggler::before {\
  content: "♦";\
}\
.multilang-toggler:hover {\
  background-color: #ddd;\
}\
.multilang-code {\
  margin: 1em;\
  border-left: 5px solid #ccc;\
  padding-left: 1em;\
}\
</style>\
  ');
  // set toggle capabilities to each chapter:
  $('.multilang-toggler').each(function() {
    $(this).siblings().hide();
    $(this).click(function() {
      $(this).siblings().toggle();
    });
  });
  // auto-expand a chapter if it is given as an anchor in the url:
  jQuery(document.location.hash).siblings()
    .css({backgroundColor:'#ff0'}).show(1000, 'linear', function() {
      jQuery(this).css({backgroundColor:'inherit'})
    });
});
})(jQuery);
</script>
<?php
  return _multilang_process(ob_get_clean());
}
/* ========================================================================== */
