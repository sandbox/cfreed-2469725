<?php
/**
 * @file
 * Multilang module file.
 *
 * Allows to embed multiple translations of the same text in a unique
 * node or user-defined block instance, using a Spip-like fashion.
 */

/* ================================================================== GENERAL */
/*
module_load_include('inc', 'multilang', 'multilang.data');
module_load_include('inc', 'multilang', 'multilang.core');

As of Drupal 7-35, the module_load_include()'s above are not executed when the
module is loaded (probably because loading now happens before full bootstrap).
So they have been inserted everywhere they are needed in hooks.
 */
/* ============================================================== BASIC HOOKS */
/**
 * Implements hook_help().
 */
function multilang_help($path, $arg) {
  switch ($path) {
    case 'admin/help#multilang':
      // Return long help (see multilang.data.inc).
      module_load_include('inc', 'multilang', 'multilang.data');
      return _multilang_help();

  }
}
/**
 * Implements hook_preprocess_block().
 *
 * Processes title and content of any (user-defined) block.
 */
function multilang_preprocess_block(&$variables) {
  if (!preg_match('#(admin/structure/block|node/\d+/edit)#i', request_uri())) {
    // (don't work when currently in edit process).
    module_load_include('inc', 'multilang', 'multilang.core');
    $variables['block']->subject
      = _multilang_process($variables['block']->subject);
    $variables['content']
      = _multilang_process($variables['content']);
  }
}
/**
 * Implements hook_preprocess_html().
 *
 * Processes possibly remaining multi blocks (node titles, breadcrumbs, Views
 * data...) and HTML <title>.
 */
function multilang_preprocess_html(&$variables) {
  if (!preg_match('`(admin/structure/block|node/\d+/edit)`i', request_uri())) {
    // (don't work when currently in edit process).
    module_load_include('inc', 'multilang', 'multilang.core');
    $variables['page']['#children']
    // For node title and breadcrumbs.
      = _multilang_process($variables['page']['#children'], /*links*/TRUE);
    $variables['head_title']
    // For HTML <title>.
      = _multilang_process($variables['head_title']);
  }
}
/* ========================================================== FORMATTER HOOKS */
/**
 * Implements hook_field_formatter_info_alter().
 *
 * For text fields, graft multilang as prioritary formatter.
 */
function multilang_field_formatter_info_alter(&$info) {
  if ($info) {
    foreach ($info as $name => $formatter) {
      if (
        !@$formatter['settings']['multilang graft']
      and
        array_intersect($formatter['field types'],
          array('text', 'text_long', 'text_with_summary'))
      ) {
        // Substitute multilang to original module.
        $info[$name]['settings']['multilang graft'] = $formatter['module'];
        $info[$name]['module'] = 'multilang';
      }
    }
  }
}
/**
 * Implements hook_field_formatter_prepare_view().
 *
 * Extract current lang part, then renew args to the original formatter.
 */
function multilang_field_formatter_prepare_view(
  $entity_type, $entities, $field, $instances, $langcode, &$items, $displays
) {
  // Reduce items to current lang part.
  if ($items) {
    module_load_include('inc', 'multilang', 'multilang.core');
    foreach ($items as $nid => $node_data) {
      if ($node_data) {
        foreach ($node_data as $delta => $item) {
          if (isset($item['summary'])) {
            $items[$nid][$delta]['summary']
              = _multilang_process($item['summary'], /*links*/TRUE);
          }
          if (isset($item['value'])) {
            $items[$nid][$delta]['value']
              = _multilang_process($item['value'], /*links*/TRUE);
          }
        }
      }
    }
  }
  // Give original formatter a chance to execute its own hook.
  foreach ($displays as $display) {
    $hook = $display['settings']['multilang graft'] .
      '_field_formatter_prepare_view';
    if (function_exists($hook)) {
      $hook(
        $entity_type, $entities, $field, $instances, $langcode, $items, $displays
      );
    }
  }
}
/**
 * Implements hook_field_formatter_view().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_view(
  $entity_type, $entity, $field, $instance, $langcode, $items, $display
) {
  $hook = $display['settings']['multilang graft'] . '_field_formatter_view';
  if (function_exists($hook)) {
    return $hook(
      $entity_type, $entity, $field, $instance, $langcode, $items, $display
    );
  }
}
/**
 * Implements hook_field_formatter_settings_summary().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_settings_summary(
  $field, $instance, $view_mode
) {
  $hook
    = $instance['display'][$view_mode]['settings']['multilang graft'] .
    '_field_formatter_settings_summary';
  if (function_exists($hook)) {
    return $hook($field, $instance, $view_mode);
  }
}
/**
 * Implements hook_field_formatter_settings_form().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_settings_form(
  $field, $instance, $view_mode, $form, &$form_state
) {
  $hook
    = $instance['display'][$view_mode]['settings']['multilang graft'] .
    '_field_formatter_settings_form';
  if (function_exists($hook)) {
    return $hook($field, $instance, $view_mode, $form, $form_state);
  }
}
/* ======================================================== AJAX-SURVEY HOOKS */
/**
 * Implements hook_init().
 */
function multilang_init() {
  if (strtolower(@$_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    // This is an Ajax request, set a flag and start buffering output.
    drupal_static('multilang_ajax_ob', TRUE);
    ob_start();
  }
}
/**
 * Implements hook_exit().
 */
function multilang_exit() {
  if (drupal_static('multilang_ajax_ob', FALSE)) {
    // Returning from Ajax request, get buffered output.
    $content = ob_get_clean();
    // Quickly look for "insert" command (avoid decoding json).
    // Pos cannot be 0 below.
    if (strpos($content, '"command":"insert"')) {
      /* Process "multi" blocks only when "insert" (resulting HTML),
      Other commands target administration dialog, let them as are */
      module_load_include('inc', 'multilang', 'multilang.core');
      $content = _multilang_process(
        str_replace(
          array('\u003C', '\u003E', '\\/'),
          array('<', '>', '/'),
          $content
        ),
        /*links*/TRUE
      );
    }
    echo $content;
  }
}
/* ============================================================== TOKEN HOOKS */
/**
 * Implements hook_token_info().
 *
 * Define [node:multilang-title] token.
 */
function multilang_token_info() {
  module_load_include('inc', 'multilang', 'multilang.data');
  module_load_include('inc', 'multilang', 'multilang.core');
  foreach (_multilang_token_info() as $token_name => $token_data) {
    // (see multilang.data.inc)
    $info['tokens']['node'][$token_name] = array(
      'name'        => _multilang_process($token_data['name']),
      'description' => _multilang_process($token_data['description']),
    );
  }
  return $info;
}
/**
 * Implements hook_tokens().
 *
 * Add token [node:multilang-title] -> current localized title.
 * Add token [node:multilang-native-title] -> native localized title.
 */
function multilang_tokens(
  $type, $tokens, array $data = array(), array $options = array()
) {
  $replacements = array();
  if ($type == 'node') {
    module_load_include('inc', 'multilang', 'multilang.core');
    foreach ($tokens as $name => $raw_token) {
      switch ($name) {
        case 'multilang-title':
          $replacements[$raw_token]
            = _multilang_process($data['node']->title);
          break;

        case 'multilang-native-title':
          $replacements[$raw_token] = _multilang_process(
            $data['node']->title, /*links*/FALSE, /*default*/TRUE
          );
          break;
      }
    }
  }
  return $replacements;
}
/* =========================================================== CKEDITOR HOOKS */
/**
 * Implements hook_element_info_alter().
 *
 * Add Drupal settings for Multilang ckeditor plugin.
 */
function multilang_element_info_alter(&$types) {
  if (@in_array(
  // Is ckeditor_pre_render_text_format defined?
    'ckeditor_pre_render_text_format', @$types['text_format']['#pre_render']
  )) {
    // Save languages list (default first).
    $default = language_default();
    $langs[$default->language] = $default->native;
    foreach (language_list() as $lang) {
      if ($lang->language <> $default->language) {
        $langs[$lang->language] = $lang->native;
      }
    }
    drupal_add_js(
      array(
        'multilang' => array(
          'langs' => $langs,
        ),
      ),
      'setting');
  }
}
/**
 * Implements hook_ckeditor_plugin().
 *
 * Add Multilang ckeditor plugin.
 */
function multilang_ckeditor_plugin() {
  module_load_include('inc', 'multilang', 'multilang.data');
  return array(
    'multilang' => array(
      'name' => 'multilang',
      'desc' => _multilang_ckeditor_plugin(),
      // (see multilang.data.inc).
      'path' => drupal_get_path('module', 'multilang') . '/ckeditor/',
      'buttons' => array(
        'multilang' => array(
          'label' => 'Multilang',
          'icon' => 'multilang.png',
        ),
      ),
    ),
  );
}
/* ========================================================================== */
